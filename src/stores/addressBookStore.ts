import { ref } from 'vue'
import { defineStore } from 'pinia'
interface addressBook {
  id: number,
  name: string,
  gender: string,
  tel: string
}
export const useAddressBookStore = defineStore('home', () => {

  let Lastid = 1
  const isAddNew = ref(false)
  const address = ref<addressBook>({
    id: 0,
    name: '',
    gender: 'Male',
    tel: ''
  })
  const addressList = ref<addressBook[]>([])
  
  function save() {
    if (address.value.id > 0) {
      const editedIndex = addressList.value.findIndex((item) => item.id === address.value.id)
      addressList.value[editedIndex] = address.value
    } else {
      addressList.value.push({ ...address.value, id: Lastid++ })
    }
    isAddNew.value = false
    address.value = {
      id: 0,
      name: '',
      gender: 'Male',
      tel: ''
    }
  }
  function edit(id: number) {
    isAddNew.value = true
    const editedIndex = addressList.value.findIndex((item) => item.id === id)
    // Copy Object JSON.parse(JSON.stringify(object))
    address.value = JSON.parse(JSON.stringify(addressList.value[editedIndex]))
  }
  
  function remove(id: number) {
    const removedIndex = addressList.value.findIndex((item) => item.id === id)
    addressList.value.splice(removedIndex, 1)
  }
  
  function cancel() {
    isAddNew.value = false
    address.value = {
      id: 0,
      name: '',
      gender: 'Male',
      tel: ''
    }
  }

  return { isAddNew, address, addressList, save, edit,  remove, cancel }
})
